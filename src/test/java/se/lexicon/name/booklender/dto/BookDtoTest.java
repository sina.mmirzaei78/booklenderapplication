package se.lexicon.name.booklender.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookDtoTest {

    @Test
    void getBookId() {
    }

    @Test
    void setBookId() {
    }

    @Test
    void getTitle() {
    }

    @Test
    void setTitle() {
    }

    @Test
    void isAvailable() {
    }

    @Test
    void setAvailable() {
    }

    @Test
    void isReserved() {
    }

    @Test
    void setReserved() {
    }

    @Test
    void getMaxLoanDays() {
    }

    @Test
    void setMaxLoanDays() {
    }

    @Test
    void getFinePerDays() {
    }

    @Test
    void setFinePerDays() {
    }

    @Test
    void getDescription() {
    }

    @Test
    void setDescription() {
    }
}