package se.lexicon.name.booklender.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

@Entity
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal loanId;
    private LocalDate loanDate;
    private boolean isTerminated;

    @ManyToOne
    @JoinColumn(name = "bookId")
    @JsonIgnore
    private Book book;

    @ManyToOne
    @JoinColumn(name = "userId")
    @JsonIgnore
    private LibraryUser loanTaker;

    public Loan(LocalDate loanDate, boolean isTerminated, Book book, LibraryUser loanTaker) {
        this.loanDate = loanDate;
        this.isTerminated = isTerminated;
        this.book = book;
        this.loanTaker = loanTaker;
    }

    public Loan() {
    }

    public BigDecimal getLoanId() {
        return loanId;
    }

    public LibraryUser getLoanTaker() {
        return loanTaker;
    }

    public void setLoanTaker(LibraryUser loanTaker) {
        this.loanTaker = loanTaker;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public BigDecimal getFine() {
        //nazanm
        LocalDate current = LocalDate.now();
        Period period = Period.between(current, loanDate);
        BigDecimal days = BigDecimal.valueOf(period.getDays());
        return (book.getFinePerDay()).multiply(days);
    }

    public boolean isOverdue() {
        if(isisTerminated())
            return false;
        LocalDate current = LocalDate.now();
        Period period = Period.between(current, loanDate);
        int days = period.getDays();
        if (book.getMaxLoanDays() > days)
            return true;
        else return false;
    }


    public LocalDate getLoanDate() {
        return loanDate;
    }

    public boolean isisTerminated() {
        return isTerminated;
    }

    public void setisTerminated(boolean isTerminated) {
        this.isTerminated = isTerminated;
    }

    public boolean extendLoan(int days) {
        if (isOverdue())
            return false;
        setisTerminated(false);
        return true;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "loanId=" + loanId +
                ", loanDate=" + loanDate +
                ", isTerminated=" + isTerminated +
                ", book=" + book +
                ", loanTaker=" + loanTaker +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return loanId == loan.loanId &&
                isTerminated == loan.isTerminated &&
                Objects.equals(loanDate, loan.loanDate) &&
                Objects.equals(book, loan.book) &&
                Objects.equals(loanTaker, loan.loanTaker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loanId, loanDate, isTerminated, book, loanTaker);
    }
}
