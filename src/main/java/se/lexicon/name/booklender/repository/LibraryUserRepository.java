package se.lexicon.name.booklender.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.name.booklender.entity.LibraryUser;

import java.util.Optional;

public interface LibraryUserRepository extends PagingAndSortingRepository<LibraryUser, Integer> {
    Optional<LibraryUser> findByEmail(String email);

}
