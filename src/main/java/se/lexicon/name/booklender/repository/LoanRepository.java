package se.lexicon.name.booklender.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.name.booklender.entity.Loan;

import java.math.BigDecimal;
import java.util.List;

public interface LoanRepository extends PagingAndSortingRepository<Loan, BigDecimal> {
    List<Loan> findAllByLoanTaker_UserId(int userId);

    List<Loan> findAllByBook_BookId(int bookId);

    List<Loan> findAllByisTerminated(boolean isTerminated);
}
