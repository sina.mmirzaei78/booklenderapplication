package se.lexicon.name.booklender.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.lexicon.name.booklender.entity.Book;

import java.util.List;

public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {
    List<Book> findAllByReserved(boolean reserved);

    List<Book> findAllByAvailable(boolean available);

    List<Book> findAllByTitle(String title);

}
