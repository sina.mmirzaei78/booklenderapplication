package se.lexicon.name.booklender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"se.lexicon.name.booklender.entity"})  // scan JPA entities
public class BooklenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooklenderApplication.class, args);
	}

}
