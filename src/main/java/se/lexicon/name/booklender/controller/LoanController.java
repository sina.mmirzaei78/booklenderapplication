package se.lexicon.name.booklender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.name.booklender.dto.LibraryUserDto;
import se.lexicon.name.booklender.dto.LoanDto;
import se.lexicon.name.booklender.service.LoanService;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class LoanController {

    @Autowired
    private LoanService loanService;

    @GetMapping("/loan/findById/{loanId}")
    public ResponseEntity findById(@PathVariable BigDecimal loanId) {
        try {
            LoanDto loanDto=loanService.findById(loanId);
            if (loanDto == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(loanDto, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/loan/findAll")
    public ResponseEntity findAll() {
        try {
            List<LoanDto> loanDtoList=loanService.findAll();
            if (loanDtoList == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(loanDtoList, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/loan/create")
    public ResponseEntity create(@RequestBody LoanDto loanDto) {
        try {
            loanService.create(loanDto);
            return new ResponseEntity(loanDto, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/loan/update")
    public ResponseEntity update(@RequestBody LoanDto loanDto) {
        try {
            LoanDto loan = loanService.findById(loanDto.getLoanId());
            if (loan == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else {
                loanService.update(loanDto);
                return new ResponseEntity(loanDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }
}
