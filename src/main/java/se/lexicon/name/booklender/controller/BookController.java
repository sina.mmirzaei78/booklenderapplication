package se.lexicon.name.booklender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.name.booklender.dto.BookDto;
import se.lexicon.name.booklender.service.BookService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(value = "/book/findById/{bookId}")
    public ResponseEntity<BookDto> findById(@PathVariable int bookId) {
        try {
            BookDto bookDto = bookService.findById(bookId);
            if (bookDto == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(bookDto, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/book/create")
    public ResponseEntity create(@RequestBody BookDto bookDto) {
        try {
            bookService.create(bookDto);
            return new ResponseEntity(bookDto, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/book/update")
    public ResponseEntity update(@RequestBody BookDto bookDto) {
        try {
            BookDto book = bookService.findById(bookDto.getBookId());
            if (book == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else {
                bookService.update(bookDto);
                return new ResponseEntity(bookDto, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/book/find")
    public ResponseEntity find (@RequestParam(name = "all",required = false,defaultValue = "0") int i,@RequestParam(name = "title",required = false) String title,
                                @RequestParam(name = "available",required = false) Options available,@RequestParam(name = "reserved",required = false) Options reserved){
        try {
            List<BookDto> bookDtoList=new ArrayList<>();
            if (title!=null){
                bookDtoList=bookService.findByTitle(title);
            }
            else if (available!=null){
                bookDtoList = bookService.findByAvailable(Boolean.valueOf(available.name));
            }
            else if (reserved!=null){
                bookDtoList = bookService.findByReserved(Boolean.valueOf(reserved.name));
            }
            else bookDtoList=bookService.findAll();
            return new ResponseEntity<>(bookDtoList, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    enum Options{
        TRUE("TRUE"),
        FALSE("FALSE");
        private final String name;
        Options(String name) {
            this.name = name;
        }
        public String toString() {
            return this.name;
        }

        public String getName() {
            return name;
        }
    }
}
