package se.lexicon.name.booklender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.lexicon.name.booklender.dto.LibraryUserDto;
import se.lexicon.name.booklender.entity.LibraryUser;
import se.lexicon.name.booklender.service.LibraryUserImp;
import se.lexicon.name.booklender.service.LibraryUserService;

import java.util.List;

@RestController
public class LibraryUserController {

    @Autowired
    private LibraryUserService libraryUserService;

    @GetMapping("/libraryUser/findUserById/{userId}")
    public ResponseEntity findById(@PathVariable int userId) {
        try {
            LibraryUserDto user = libraryUserService.findById(userId);
            if (user == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(user, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/libraryUser/findUserByEmail/{email}")
    public ResponseEntity findByEmail(@PathVariable String email) {
        try {
            LibraryUserDto user = libraryUserService.findByEmail(email);
            if (user == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(user, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/libraryUser/findAllUsers")
    public ResponseEntity findAll() {
        try {
            List<LibraryUserDto> usersList = libraryUserService.findAll();
            if (usersList == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else return new ResponseEntity(usersList, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/libraryUser/create")
    public ResponseEntity create(@RequestBody LibraryUserDto libraryUserDto) {
        try {
            if (libraryUserService.findByEmail(libraryUserDto.getEmail()) == null) {
                libraryUserService.create(libraryUserDto);
                return new ResponseEntity(libraryUserDto, HttpStatus.CREATED);
            } else return new ResponseEntity(HttpStatus.IM_USED);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/libraryUser/update")
    public ResponseEntity update(@RequestBody LibraryUserDto libraryUserDto) {
        try {
            LibraryUserDto userDto = libraryUserService.findById(libraryUserDto.getUserId());
            if (userDto == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else {
                if (libraryUserService.findByEmail(libraryUserDto.getEmail()) != null && !libraryUserDto.getEmail().equals(userDto.getEmail())) {
                    return new ResponseEntity(HttpStatus.IM_USED);
                } else {
                    libraryUserService.update(libraryUserDto);
                    return new ResponseEntity(libraryUserDto, HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }
}
