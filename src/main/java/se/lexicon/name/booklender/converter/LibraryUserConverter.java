package se.lexicon.name.booklender.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.lexicon.name.booklender.dto.LibraryUserDto;
import se.lexicon.name.booklender.entity.LibraryUser;
import se.lexicon.name.booklender.repository.LibraryUserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LibraryUserConverter {

    @Autowired
    private static LibraryUserRepository libraryUserRepository;

    public LibraryUserDto entityToDto(LibraryUser libraryUser) {
        LibraryUserDto libraryUserDto = new LibraryUserDto();
        libraryUserDto.setEmail(libraryUser.getEmail());
        libraryUserDto.setName(libraryUser.getName());
        libraryUserDto.setUserId(libraryUser.getUserId());
        libraryUserDto.setRegDate(libraryUser.getRegDate());
        return libraryUserDto;
    }

    public LibraryUser dtoToEntity(LibraryUserDto libraryUserDto) {
        LibraryUser libraryUser=new LibraryUser(libraryUserDto.getRegDate(),libraryUserDto.getName(),libraryUserDto.getEmail());
        return libraryUser;
    }

    public List<LibraryUserDto> entityListToDtoList(List<LibraryUser> libraryUserList) {
        return libraryUserList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public List<LibraryUser> dtoListToEntityList(List<LibraryUserDto> libraryUserDtoList) {
        return libraryUserDtoList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
