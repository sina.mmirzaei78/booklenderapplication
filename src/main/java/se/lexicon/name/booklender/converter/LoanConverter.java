package se.lexicon.name.booklender.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.lexicon.name.booklender.dto.LoanDto;
import se.lexicon.name.booklender.entity.Loan;
import se.lexicon.name.booklender.repository.BookRepository;
import se.lexicon.name.booklender.repository.LoanRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoanConverter {

    @Autowired
    private static LoanRepository loanRepository;
    @Autowired
    private BookConverter bookConverter;
    @Autowired
    private LibraryUserConverter libraryUserConverter;

    public LoanDto entityToDto(Loan loan) {
        LoanDto loanDto= new LoanDto();
        loanDto.setLoanId(loan.getLoanId());
        loanDto.setBook(bookConverter.entityToDto(loan.getBook()));
        loanDto.setLoanDate(loan.getLoanDate());
        loanDto.setLoanTaker(libraryUserConverter.entityToDto(loan.getLoanTaker()));
        loanDto.setisTerminated(loan.isisTerminated());
        return loanDto;
    }

    public Loan dtoToEntity(LoanDto loanDto) {
        Loan loan = new Loan(loanDto.getLoanDate(),loanDto.isisTerminated(),bookConverter.dtoToEntity(loanDto.getBook()),libraryUserConverter.dtoToEntity(loanDto.getLoanTaker()));
        return loan;
    }

    public List<LoanDto> entityListToDtoList(List<Loan> loanList) {
        return loanList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public List<Loan> dtoListToEntityList(List<LoanDto> loanDtoList) {
        return loanDtoList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
