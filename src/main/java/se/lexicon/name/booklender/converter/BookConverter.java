package se.lexicon.name.booklender.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.lexicon.name.booklender.dto.BookDto;
import se.lexicon.name.booklender.entity.Book;
import se.lexicon.name.booklender.repository.BookRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BookConverter {

    @Autowired
    private static BookRepository bookRepository;

    public BookDto entityToDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setBookId(book.getBookId());
        bookDto.setTitle(book.getTitle());
        bookDto.setAvailable(book.isAvailable());
        bookDto.setReserved(book.isReserved());
        bookDto.setMaxLoanDays(book.getMaxLoanDays());
        bookDto.setFinePerDays(book.getFinePerDay());
        bookDto.setDescription(book.getDescription());
        return bookDto;
    }

    public Book dtoToEntity(BookDto bookDto) {
        Book book=new Book(bookDto.getTitle(),bookDto.getMaxLoanDays(),bookDto.getFinePerDays(),bookDto.getDescription());
        book.setReserved(bookDto.isReserved());
        book.setAvailable(bookDto.isAvailable());
        return book;
    }

    public List<BookDto> entityListToDtoList(List<Book> bookList) {
        return bookList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public List<Book> dtoListToEntityList(List<BookDto> bookDtoList) {
        return bookDtoList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }

}
