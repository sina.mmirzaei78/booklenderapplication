package se.lexicon.name.booklender.service;

import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.dto.LoanDto;

import java.math.BigDecimal;
import java.util.List;

@Service
public interface LoanService {
    LoanDto findById(BigDecimal loanId);
    List<LoanDto> findByBookId(int bookId);
    List<LoanDto> findByUserId(int userId);
    List<LoanDto> findByisTerminated(boolean isisTerminated);
    List<LoanDto> findAll();
    LoanDto create(LoanDto loanDto);
    LoanDto update(LoanDto loanDto);
    boolean delete(BigDecimal loanId);
}
