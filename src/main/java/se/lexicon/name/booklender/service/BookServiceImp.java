package se.lexicon.name.booklender.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.converter.BookConverter;
import se.lexicon.name.booklender.dto.BookDto;
import se.lexicon.name.booklender.entity.Book;
import se.lexicon.name.booklender.repository.BookRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BookServiceImp implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookConverter bookConverter;

    @Override
    public List<BookDto> findByReserved(boolean isReserved) {
        List<Book> bookList = bookRepository.findAllByReserved(isReserved);
        if (bookList.isEmpty())
            return null;
        else
            return bookConverter.entityListToDtoList(bookList);
    }

    @Override
    public List<BookDto> findByAvailable(boolean isAvailable) {
        List<Book> bookList = bookRepository.findAllByAvailable(isAvailable);
        if (bookList.isEmpty())
            return null;
        else
            return bookConverter.entityListToDtoList(bookList);
    }

    @Override
    public List<BookDto> findByTitle(String title) {
        List<Book> bookList = bookRepository.findAllByTitle(title);
        if (bookList.isEmpty())
            return null;
        else
            return bookConverter.entityListToDtoList(bookList);
    }

    @Override
    public BookDto findById(int bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (!book.isPresent())
            return bookConverter.entityToDto(book.get());
        else return null;
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> bookList = (List<Book>) bookRepository.findAll();
        if (bookList.isEmpty())
            return null;
        else
            return bookConverter.entityListToDtoList(bookList);
    }

    @Override
    public BookDto create(BookDto bookDto) {
        Book book = bookConverter.dtoToEntity(bookDto);
        System.out.println(book.getTitle());
        bookRepository.save(book);
        return bookDto;
    }

    @Override
    public BookDto update(BookDto bookDto) {
        Book book = bookConverter.dtoToEntity(bookDto);
        bookRepository.save(book);
        return bookDto;
    }

    @Override
    public boolean delete(int bookId) {
        Book book = bookRepository.findById(bookId).get();
        bookRepository.delete(book);
        return true;
    }
}
