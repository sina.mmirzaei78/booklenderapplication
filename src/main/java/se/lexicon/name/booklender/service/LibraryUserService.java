package se.lexicon.name.booklender.service;

import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.dto.LibraryUserDto;

import java.util.List;

@Service
public interface LibraryUserService {
    LibraryUserDto findById(int userId);
    LibraryUserDto findByEmail(String email);
    List<LibraryUserDto> findAll();
    LibraryUserDto create(LibraryUserDto libraryUserDto);
    LibraryUserDto update(LibraryUserDto libraryUserDto);
    boolean delete(int userId);
}
