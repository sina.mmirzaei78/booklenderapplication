package se.lexicon.name.booklender.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.converter.LoanConverter;
import se.lexicon.name.booklender.dto.LoanDto;
import se.lexicon.name.booklender.entity.Book;
import se.lexicon.name.booklender.entity.Loan;
import se.lexicon.name.booklender.repository.LoanRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class LoanServiceImp implements LoanService {

    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private LoanConverter loanConverter;

    @Override
    public LoanDto findById(BigDecimal loanId) {
        Optional<Loan> loan = loanRepository.findById(loanId);
        if (!loan.isPresent())
            return null;
        else
            return loanConverter.entityToDto(loanRepository.findById(loanId).get());
    }

    @Override
    public List<LoanDto> findByBookId(int bookId) {
        List<Loan> loanList = loanRepository.findAllByBook_BookId(bookId);
        if (loanList.isEmpty())
            return null;
        else
            return loanConverter.entityListToDtoList(loanList);
    }

    @Override
    public List<LoanDto> findByUserId(int userId) {
        List<Loan> loanList = loanRepository.findAllByLoanTaker_UserId(userId);
        if (loanList.isEmpty())
            return null;
        else
            return loanConverter.entityListToDtoList(loanList);
    }

    @Override
    public List<LoanDto> findByisTerminated(boolean isisTerminated) {
        List<Loan> loanList = loanRepository.findAllByisTerminated(isisTerminated);
        if (loanList.isEmpty())
            return null;
        else
            return loanConverter.entityListToDtoList(loanList);
    }

    @Override
    public List<LoanDto> findAll() {
        List<Loan> loanList = (List<Loan>) loanRepository.findAll();
        if (loanList.isEmpty())
            return null;
        else
            return loanConverter.entityListToDtoList(loanList);
    }

    @Override
    public LoanDto create(LoanDto loanDto) {
        loanDto.setLoanDate(LocalDate.now());
        Loan loan = loanConverter.dtoToEntity(loanDto);
        loanRepository.save(loan);
        return loanDto;
    }

    @Override
    public LoanDto update(LoanDto loanDto) {
        Loan loan = loanConverter.dtoToEntity(loanDto);
        loanRepository.save(loan);
        return loanDto;
    }

    @Override
    public boolean delete(BigDecimal loanId) {
        Optional<Loan> loan = loanRepository.findById(loanId);
        if (!loan.isPresent()) {
            return false;
        }
        loanRepository.delete(loan.get());
        return true;
    }
}
