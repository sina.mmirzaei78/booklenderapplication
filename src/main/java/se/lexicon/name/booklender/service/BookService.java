package se.lexicon.name.booklender.service;

import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.dto.BookDto;

import java.util.List;

@Service
public interface BookService {
    List<BookDto> findByReserved(boolean isReserved);
    List<BookDto> findByAvailable(boolean isAvailable);
    List<BookDto> findByTitle(String title);
    BookDto findById(int bookId);
    List<BookDto> findAll();
    BookDto create(BookDto bookDto);
    BookDto update(BookDto bookDto);
    boolean delete(int bookId);
}
