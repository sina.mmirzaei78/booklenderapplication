package se.lexicon.name.booklender.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import se.lexicon.name.booklender.converter.LibraryUserConverter;
import se.lexicon.name.booklender.dto.LibraryUserDto;
import se.lexicon.name.booklender.entity.LibraryUser;
import se.lexicon.name.booklender.repository.LibraryUserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class LibraryUserImp implements LibraryUserService {

    @Autowired
    private LibraryUserRepository libraryUserRepository;
    @Autowired
    private LibraryUserConverter libraryUserConverter;

    @Override
    public LibraryUserDto findById(int userId) {
        Optional<LibraryUser> user = libraryUserRepository.findById(userId);
        if (user.isPresent())
            return libraryUserConverter.entityToDto(user.get());
        else return null;
    }

    @Override
    public LibraryUserDto findByEmail(String email) {
        Optional<LibraryUser> user = libraryUserRepository.findByEmail(email);
        if (user.isPresent())
            return libraryUserConverter.entityToDto(user.get());
        else return null;
    }

    @Override
    public List<LibraryUserDto> findAll() {
        List<LibraryUser> libraryUserList = (List<LibraryUser>) libraryUserRepository.findAll();
        if (libraryUserList.isEmpty())
            return null;
        else return libraryUserConverter.entityListToDtoList(libraryUserList);
    }

    @Override
    public LibraryUserDto create(LibraryUserDto libraryUserDto) {
        libraryUserDto.setRegDate(LocalDate.now());
        LibraryUser libraryUser = libraryUserConverter.dtoToEntity(libraryUserDto);
        libraryUserRepository.save(libraryUser);
        return libraryUserDto;
    }

    @Override
    public LibraryUserDto update(LibraryUserDto libraryUserDto) {
        LibraryUser libraryUser = libraryUserConverter.dtoToEntity(libraryUserDto);
        libraryUserRepository.save(libraryUser);
        return libraryUserDto;
    }

    @Override
    public boolean delete(int userId) {
        Optional<LibraryUser> libraryUser = libraryUserRepository.findById(userId);
        if (!libraryUser.isPresent()) {
            return false;
        }
        libraryUserRepository.delete(libraryUser.get());
        return true;
    }
}
