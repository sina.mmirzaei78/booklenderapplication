package se.lexicon.name.booklender.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LoanDto {
    private BigDecimal loanId;
    private LibraryUserDto loanTaker;
    private BookDto book;
    private LocalDate loanDate;
    private boolean isTerminated;

    public BigDecimal getLoanId() {
        return loanId;
    }

    public void setLoanId(BigDecimal loanId) {
        this.loanId = loanId;
    }

    public LibraryUserDto getLoanTaker() {
        return loanTaker;
    }

    public void setLoanTaker(LibraryUserDto loanTaker) {
        this.loanTaker = loanTaker;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public LocalDate getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(LocalDate loanDate) {
        this.loanDate = loanDate;
    }

    public boolean isisTerminated() {
        return isTerminated;
    }

    public void setisTerminated(boolean isTerminated) {
        this.isTerminated = isTerminated;
    }
}
